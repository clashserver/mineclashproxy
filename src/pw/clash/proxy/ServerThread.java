package pw.clash.proxy;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

import pw.clash.proxy.packets.Packet;
import pw.clash.proxy.packets.PacketInfo;
import pw.clash.proxy.packets.PacketInfo.InfoType;
import pw.clash.proxy.packets.PacketJoin;
import pw.clash.proxy.utils.Log;

public class ServerThread extends Thread{
	
	private Socket socket;
	
	private ObjectInputStream input;
	private ObjectOutputStream output;
	
	private int port;
	private int slots;
	
	private String ip;
	
	private boolean acceptingPlayers = true;
	
	public ServerThread(Socket s) {
		socket = s;
		ip = socket.getInetAddress().toString().replace("/", "");
		try {
			input = new ObjectInputStream(socket.getInputStream());
			output = new ObjectOutputStream(socket.getOutputStream());
		} catch (IOException e) {
			Log.error("Creating streams - " + e.getLocalizedMessage(), e.getMessage());
		}
		Proxy.OPEN_SERVERS.add(this);
	}
	
	@Override
	public void run() {
		while(true) {
			try {
				Object o = input.readObject();
				if(o instanceof Packet) {
					if(o instanceof PacketJoin) {
						PacketJoin j = (PacketJoin) o;
						this.port = j.getPort();
						this.slots = j.getSlots();
						Log.info("Server " + ip, "Port: " + port + ", Slots: " + slots);
					}
					
					if(o instanceof PacketInfo) {
						PacketInfo f = (PacketInfo) o;
						if(f.getType() == InfoType.FULL) {
							acceptingPlayers = false;
							while(Proxy.OPEN_SERVERS.contains(this))Proxy.OPEN_SERVERS.remove(this);
							Log.info("Server Full " + port, "Done.");
							break;
						} else if(f.getType() == InfoType.CLOSED) {
							acceptingPlayers = false;
							while(Proxy.OPEN_SERVERS.contains(this))Proxy.OPEN_SERVERS.remove(this);
							Log.info("Server Closed " + port, "Done.");
							break;
						} else if(f.getType() == InfoType.STARTED) {
							acceptingPlayers = false;
							while(Proxy.OPEN_SERVERS.contains(this))Proxy.OPEN_SERVERS.remove(this);
							Log.info("Server Started " + port, "Done.");
							break;
						}
					}
				}
			} catch(Exception e) {
				Log.error("Receiving bytes - " + e.getLocalizedMessage(), e + "");
				break;
			}
		}
		try {
			disconnect();
		} catch (IOException e) {
			Log.error("Disconnect error - " + e.getLocalizedMessage(), e.getMessage());
		}
	}
	
	public void disconnect() throws IOException {
		while(Proxy.OPEN_SERVERS.contains(this))Proxy.OPEN_SERVERS.remove(this);
		if(output != null) {
			output.close();
		}
		
		if(input != null) {
			input.close();
		}
		
		if(socket != null) {
			socket.close();
		}
	}
	
	public int getPort() {
		return port;
	}
	
	public int getSlots() {
		return slots;
	}
	
	public String getIP() {
		return ip;
	}
	
	public ObjectInputStream getInput() {
		return input;
	}
	
	public ObjectOutputStream getOutput() {
		return output;
	}
	
	public boolean isAccepting() {
		return acceptingPlayers;
	}
	
	public void sendPacket(Packet p) {
		try {
			output.writeObject(p);
		} catch (IOException e) {
			Log.error("Writing Packet - " + e.getLocalizedMessage(), e.getMessage());
		}
	}
	
}
