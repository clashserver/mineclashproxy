package pw.clash.proxy;

import java.io.DataInputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.ArrayList;

import pw.clash.proxy.utils.Log;


public class Proxy {
	
	private int serverPort;
	private int clientPort;
	
	public static ArrayList<ServerThread> OPEN_SERVERS = new ArrayList<ServerThread>();
	
	public Proxy(int clientPort, int serverPort) {
		this.clientPort = clientPort;
		this.serverPort = serverPort;
	}
	
	public void start() {
		new Thread() {
			@Override
			public void run() {
				try {
					ServerSocket serverSocket = new ServerSocket(serverPort);
					while(true) {
						ServerThread s = new ServerThread(serverSocket.accept());
						s.start();
					}
				} catch (IOException e) {
					Log.error("Starting Server ServerSocket - " + e.getLocalizedMessage(), e.getMessage());
				}
			}
		}.start();
		
		new Thread() {
			
			public void run() {
				try {
					ServerSocket clientSocket = new ServerSocket(clientPort);
					while(true) {
						new PlayerThread(clientSocket.accept()).start();
					}
				} catch (IOException e) {
					Log.error("Creating Client ServerSocket - " + e.getLocalizedMessage(), e.getMessage());
				}
			};
			
		}.start();
	}
	
	public static void main(String[] args) {
		new Proxy(1821, 2003).start();
		Log.info("Starting up...", "Successful.");
	}
	
	class PlayerThread extends Thread {
		
		Socket s;
		
		public PlayerThread(Socket s) {
			this.s = s;
			try {
				Socket server;
				if(OPEN_SERVERS.size() == 0) {
					server = new Socket("127.0.0.1", 1020);
				} else {
					server = new Socket(OPEN_SERVERS.get(0).getIP(), OPEN_SERVERS.get(0).getPort());
				}
				new Player(s, server).start();
				new Server(s, server).start();
			} catch (IOException e) {
				Log.error(e.getLocalizedMessage(), e + "");
			}
		}
		
	}
	
	class Player extends Thread {
		
		Socket client;
		Socket server;
		
		DataInputStream input;
		DataOutputStream output;
		
		boolean start = false;
		
		public Player(Socket client, Socket server) {
			try {
				input = new DataInputStream(server.getInputStream());
				output = new DataOutputStream(client.getOutputStream());
			} catch(Exception e) {
				Log.error(e.getLocalizedMessage(), e + "");
				return;
			}
			start = true;
			Log.info(client.getInetAddress().getHostName(), "Connected.");
		}
		
		@Override
		public void run() {
			while(start) {
				try {
					Byte o = input.readByte();
					output.writeByte(o);
				} catch (IOException e) {
					Log.error(e.getLocalizedMessage(), e + "");
					break;
				}
			}
		}
		
	}
	
	class Server extends Thread {
		
		Socket client;
		Socket server;
		
		DataInputStream input;
		DataOutputStream output;
		
		boolean start = false;
		
		public Server(Socket client, Socket server) {
			try {
				input = new DataInputStream(client.getInputStream());
				output = new DataOutputStream(server.getOutputStream());				
			} catch(Exception e) {
				Log.error(e.getLocalizedMessage(), e + "");
				return;
			}
			start = true;
		}
		
		@Override
		public void run() {
			while(start) {
				try {
					Byte o = input.readByte();
					output.writeByte(o);
				} catch (IOException e) {
					Log.error(e.getLocalizedMessage(), e + "");
					break;
				}
			}
		}
		
	}
	
}
