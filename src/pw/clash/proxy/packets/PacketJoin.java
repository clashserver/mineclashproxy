package pw.clash.proxy.packets;

public class PacketJoin extends Packet{

	/**
	 * 
	 */
	private static final long serialVersionUID = 8901385408395070807L;

	private int slots = 0;
	private int port = 0;
	
	public PacketJoin(int slots, int port) {
		this.slots = slots;
		this.port = port;
	}
	
	@Override
	public int getID() {
		return 0;
	}

	@Override
	public String getName() {
		return "PacketJoin";
	}
	
	public int getSlots() {
		return slots;
	}
	
	public int getPort() {
		return port;
	}

}
