package pw.clash.proxy.packets;

public class PacketInfo extends Packet {

	/**
	 * 
	 */
	private static final long serialVersionUID = -4056545025330884050L;

	public enum InfoType {
		FULL,
		CLOSED,
		STARTED
	}
	
	private InfoType type;
	
	public PacketInfo(InfoType t) {
		this.type = t;
	}
	
	@Override
	public int getID() {
		return 2;
	}

	@Override
	public String getName() {
		return "PacketInfo";
	}
	
	public InfoType getType() {
		return type;
	}

}
