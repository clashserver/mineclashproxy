package pw.clash.proxy.packets;

import java.io.Serializable;

public abstract class Packet implements Serializable{
	
	/**
	 * 
	 */
	private static final long serialVersionUID = 6253463466225149252L;

	public abstract int getID();
	public abstract String getName();
	
}
