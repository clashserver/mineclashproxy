package pw.clash.proxy.utils;

public class Log {
	
	public static void info(String tag, String message) {
		System.out.println("INFO: " + tag + " - " + message);
	}
	
	public static void error(String tag, String message) {
		System.out.println("ERROR: " + tag + " - " + message);
	}
	
}
